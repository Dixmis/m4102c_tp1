/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.iutamiens.m4102c.tpchat_berges_gennevee;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Dixmi
 */
public class ClientChat {
    private Socket socket;
    private IOCommandes ioCommandes;
    
    public ClientChat() {
        socket = null;
        try {
            socket = new Socket("172.16.1.64", 6666);
        } catch (UnknownHostException ex) {
            System.out.println(ex.getMessage());
            return;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        
        ioCommandes = new IOCommandes(socket);
    }
    
    private void commandeUsers() {
        ioCommandes.ecrireReseau("USERS");
        String firstLine = ioCommandes.lireReseau();
        int nbrLine = Integer.parseInt(firstLine.split(" ")[0].split(":")[1]);
        if (nbrLine == 0) ioCommandes.ecrireEcran("Pas d'autre utilisateur connecté");
        for (int i = 0; i < nbrLine; i++) {
            ioCommandes.ecrireEcran(ioCommandes.lireReseau());
        }
    }
    
    private void commandeMessages() {
        ioCommandes.ecrireReseau("MESSAGES");
        String firstLine = ioCommandes.lireReseau();
        int nbrLine = Integer.parseInt(firstLine.split(" ")[0].split(":")[1]);
        if (nbrLine == 0) ioCommandes.ecrireEcran("Pas de message en attente");
        for (int i = 0; i < nbrLine; i++) {
            ioCommandes.ecrireEcran(ioCommandes.lireReseau());
        }
    }
    
    private void commandeEnvoieDiffuse(String text) {
        ioCommandes.ecrireReseau("SEND: " + text);
        ioCommandes.lireReseau();
    }
    
    private void commandeEnvoieIndividuel(String text) {
        String[] tableTextToSend = text.substring(4).split(" ");
        String desti = tableTextToSend[0];
        String textToSend = "";
        for (int i = 1; i < tableTextToSend.length; i++) {
            textToSend += tableTextToSend[i] + " ";
        }
        if (!textToSend.isEmpty()) {
            ioCommandes.ecrireReseau("SEND " + desti + ":" + textToSend);
        }
        ioCommandes.lireReseau();
    }
    
    private void commandeHelp() {
        ioCommandes.ecrireEcran("/h pour l'aide");
        ioCommandes.ecrireEcran("/m pour afficher les messages en attente");
        ioCommandes.ecrireEcran("/u pour afficher la liste des utilisateur connectés");
        ioCommandes.ecrireEcran("/pv <pseudo> <message> pour envoyer un message privé à <pseudo>");
        ioCommandes.ecrireEcran("/quit pour quitter");
        ioCommandes.ecrireEcran("Sinon, pour envoyer un message, juste écrivez le :)");
    }
    
    private void commandeQuit() {
        ioCommandes.ecrireReseau("QUIT");
        ioCommandes.ecrireEcran("Déconnecté du serveur");
        ioCommandes.ecrireEcran("Au revoir :3");
    }
    
    public void dialogueConsole() {
        ioCommandes.ecrireEcran("Bonjour et bienvenue sur TPCHAT !!");
        ioCommandes.lireReseau();
        String pseudo = "";
        do {
            ioCommandes.ecrireEcran("Notez votre pseudo :");
            pseudo = ioCommandes.lireEcran();
            ioCommandes.ecrireReseau("USER:<" + pseudo + ">");
        } while (!ioCommandes.lireReseau().startsWith("OK"));
        ioCommandes.ecrireEcran("Vous pouvez maintenant écrire dans le chat (/h pour l'aide)");
        String text = ioCommandes.lireEcran();
        while (!text.equalsIgnoreCase("/quit") && !text.equalsIgnoreCase("/q")) {
            if (!text.isEmpty()) {
                if (text.startsWith("/")) {
                    if (text.equals("/m")) {
                        commandeMessages();
                    } else if (text.startsWith("/pv")) {
                        commandeEnvoieIndividuel(text);
                    } else if (text.equals("/u")) {
                        commandeUsers();
                    } else {
                        commandeHelp();
                    }
                } else {
                    commandeEnvoieDiffuse(text);
                }
            }
            text = ioCommandes.lireEcran();
        }
        commandeQuit();
    }
}
